import os
import boto3

from python_terraform import *


def lambda_handler(event, context):
    os.system('cp /var/task/{*.tf,lambda_function.py} /tmp/')
    
    # Set below in Env variables in lambda

    action = os.environ['ACTION']
    region = os.environ['REGION']

    # Get ami-latest image param from SSM param   
    ami_id = get_ami_id(region)
    print(ami_id)

    # Execute the terraform scripts to install Inspector agent 
    output = execute(region,ami_id)
    template_arn = output['template_arn']['value']
    print("template_arn ===== ", template_arn)

    # Start Inspector assesment run
    start_assessment_run(region,template_arn)

    
def get_ami_id(region):
    client = boto3.client('ssm',region)
    
    response = client.get_parameter(
    Name='ami-latest',
    WithDecryption=True
    )
    return (response['Parameter']['Value'])
    
    
def start_assessment_run(region, arn):
    client = boto3.client('inspector',region)

    response = client.start_assessment_run(
        assessmentTemplateArn=arn,
        assessmentRunName='Gold_AMI_Assessment_Run'
    )
    
    
def execute(region,ami_id):
        print("In Execution() ---",ami_id)
        tf = Terraform(working_dir="/tmp/",terraform_bin_path='/opt/terraform',variables={"region": region, "AMI_ID": ami_id})
        tf.init()
        approve = {"auto-approve": True}
        tf.apply(capture_output=True, skip_plan=True, **approve)
        stdout=tf.output()
        return stdout